import express, { Application } from "express";
import bodyParser from "body-parser";
import HealthCheckRoute from './health/healthCheckRoute';
import ContactRoute from './contacts/contactRoute';

const app: Application = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api/v1/health', HealthCheckRoute);
app.use('/api/v1/contacts', ContactRoute);

export default app;