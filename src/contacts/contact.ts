import * as mongoose from 'mongoose';

/**
 * 	{
		"firstname": "My firstname",
		"lastname": "my lastname",
		"email": "my email",
		"phone": "my phone number"
	}
 */

const schema = new mongoose.Schema({
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  email: {
    type: String
  },
  phone: {
    type: String
  }
});

export const model = mongoose.model("Contact", schema);