import { Request, Response } from 'express';
import * as contactRepository from './contactRepository';

export const getContacts = async (req: Request, res: Response) => {
  const contacts = await contactRepository.getContacts();
  // some logic
  res.json(contacts);
};

export const createContact = async (req: Request, res: Response) => {

}