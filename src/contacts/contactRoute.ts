import express from 'express';
import { getContacts, createContact } from './contactController';

const router = express.Router();

router.get('/', getContacts);
router.post('/', createContact);

export default router;