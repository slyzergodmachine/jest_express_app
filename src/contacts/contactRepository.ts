import { model } from './contact';

export const getContacts = async () => {
  return model.find();
}
