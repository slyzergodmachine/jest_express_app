import mongoose from 'mongoose';

// Return true if the service is healthy
export const healthCheck = async () => {
  // TODO: Implement perform health check like database call and etc.
  /**
   * 0: disconnected
     1: connected
     2: connecting
     3: disconnecting
   */
  return mongoose.connection.readyState === 1;
}