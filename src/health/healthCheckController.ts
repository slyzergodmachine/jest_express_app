import { Request, Response } from 'express';
import * as healthCheckService from './healthCheckService';

export const getHealthCheck = async (req: Request, res: Response) => {
  const isServiceHealthy = await healthCheckService.healthCheck();
  res.json({ 
    status: isServiceHealthy ? 'healthy' : 'unhealthy'
  });
}