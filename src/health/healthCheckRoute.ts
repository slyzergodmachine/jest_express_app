import express from 'express';
import { getHealthCheck } from './healthCheckController';

const router = express.Router();

router.get('/', getHealthCheck);

export default router;