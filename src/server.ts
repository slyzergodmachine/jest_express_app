import app from './app';
import mongoose from "mongoose";

const port: number = 5001 || process.env.PORT;
const databaseURI = 'mongodb://localhost:27017/testcontact';

const connectToDB = () => {
  mongoose.connect(databaseURI, { useNewUrlParser: true, useUnifiedTopology: true })
        .then(() => {
          return console.log("Connect success !")
        })
        .catch(() => {
          return process.exit(1);
        })
        ;
};
connectToDB();
app.listen(port, () => {
  console.log(`Server running on ${port}`);
});