import supertest from 'supertest';
import * as contactRepository from '../../src/contacts/contactRepository';
import app from '../../src/app';
const request = supertest(app);

jest.mock('../../src/contacts/contactRepository');
describe('ContactController', () => {
  describe('getContacts', () => {
    test('When contact exists, then it should return a list of contacts and status code 200', async () => {
      // Arrange
      const getContactSub = [
        {
            "_id": "5efb154da46d24c94f32692f",
            "firstname": "My firstname",
            "lastname": "my lastname",
            "email": "my email",
            "phone": "my phone number"
        }
      ];
      (contactRepository.getContacts as jest.Mock).mockImplementation(async () => {
        return getContactSub
      });

      // Act
      const actual = await request.get('/api/v1/contacts');

      // Assert
      expect(actual.body).toStrictEqual(getContactSub);
      expect(actual.status).toBe(200);
    });

    test('When no contact exists, then it should return empty list and status code 200', async () => {
      // Arrange
      const getContactSub: any = [];
      (contactRepository.getContacts as jest.Mock).mockImplementation(async () => {
        return getContactSub
      });

      // Act
      const actual = await request.get('/api/v1/contacts');

      // Assert
      expect(actual.body).toStrictEqual(getContactSub);
      expect(actual.status).toBe(200);
    });

    // TODO: Add another error case
  });
});