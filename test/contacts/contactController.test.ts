import * as contactController from '../../src/contacts/contactController';
import * as contactRepository from '../../src/contacts/contactRepository';

jest.mock('../../src/contacts/contactRepository');
describe('ContactController', () => {
  describe('getContacts', () => {
    test('When contact exists, it should return a list of contacts', async () => {
      // Arrange
      const spyResponse = createSpyResponse();
      const getContactSub = [
        {
            "_id": "5efb154da46d24c94f32692f",
            "firstname": "My firstname",
            "lastname": "my lastname",
            "email": "my email",
            "phone": "my phone number"
        }
      ];
      (contactRepository.getContacts as jest.Mock).mockImplementation(async () => {
        return getContactSub
      });

      // Act 
      await contactController.getContacts({} as any, spyResponse);

      // Assert
      expect(spyResponse.json).toBeCalledWith(getContactSub);
    });
  });
});

const createSpyResponse: any = () => {
  const res: any = {};
  // Mimic express response behavior
  res.json = jest.fn().mockReturnValue(res);
  return res;
};