import supertest from 'supertest';
import * as healthCheckService from '../../src/health/healthCheckService';

import app from '../../src/app';;
const request = supertest(app);

jest.mock('../../src/health/healthCheckService');

beforeAll(() => {
  jest.resetAllMocks();
})

describe('HealthCheckController', () => {
  describe('getHealthCheck', () => {
    test('When the service is ready, then it should return status healthy', async () => {
      // Arrange
      (healthCheckService.healthCheck as jest.Mock).mockReturnValue(true);
      // Act
      const actual = await request.get('/api/v1/health');
      // Assert
      expect(actual.body.status).toBe('healthy');
    });

    test('When the database or service is not ready, it should return status unhealthy', async () => {
      // Arrange
      (healthCheckService.healthCheck as jest.Mock).mockReturnValue(false);
      // Act
      const actual = await request.get('/api/v1/health');
      // Assert
      expect(actual.body.status).toBe('unhealthy');
    });
    // TODO: Add another error cases here
  });
});