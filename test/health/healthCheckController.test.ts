import * as healthCheckController from '../../src/health/healthCheckController';
import * as healthCheckService from '../../src/health/healthCheckService';

jest.mock('../../src/health/healthCheckService');

beforeAll(() => {
  jest.resetAllMocks();
})

describe('HealthCheckController', () => {
  describe('getHealthCheck', () => {
    test('When the service is ready, it should return status healthy', async () => {
      // Arrange
      const mockResponse = createMockResponse();
      (healthCheckService.healthCheck as jest.Mock).mockReturnValue(true);
      // Act 
      await healthCheckController.getHealthCheck({} as any, mockResponse);
      // Assert
      expect(mockResponse.json).toBeCalledWith({
        status: 'healthy'
      });
    });

    test('When the database or service is not ready, it should return status unhealthy', async () => {
      // Arrange
      const mockResponse = createMockResponse();
      (healthCheckService.healthCheck as jest.Mock).mockReturnValue(false);
      // Act 
      await healthCheckController.getHealthCheck({} as any, mockResponse);
      // Assert
      expect(mockResponse.json).toBeCalledWith({
        status: 'unhealthy'
      });
    });

    // TODO: Add another error cases here
  });
});

const createMockResponse: any = () => {
  const res: any = {};
  res.json = jest.fn().mockReturnValue(res);
  return res;
};